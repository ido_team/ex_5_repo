<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;

class StudentController extends Controller
{
    public function actionView($id)
    {
        $model = Student::findOne($id);
		$name = $model->getName();
		return $this->render('showOne' , ['name' => $name]);
    }
}

