<?php

	namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;	
	
	class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
	{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

	public function rules()
    {
        return [
            [['username', 'password', 'auth_key', ], 'string', 'max' => 255],
			[['firstname', 'lastname', 'phone', ], 'string', 'max' => 255],
			['email', 'email'],
			[['username', 'password', ], 'required'],
			[['created_at', 'updated_at', ], 'integer'],
			[['created_by', 'updated_by', ], 'integer'],
            [['username'], 'unique'],			
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
			'firstname' => 'First name',
			'lastname'  => 'Last name',
			'email'  => 'Email',
			'phone'  => 'Phone number',
        ];
    }	
	

	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }


	public static function getUsers()
	{
		$allUsers = self::find()->all();
		$usersFirstname = ArrayHelper::
					map($allUsers, 'id', 'firstname');
		$usersLastname = ArrayHelper::
					map($allUsers, 'id', 'lastname');
		$users = []; 
		foreach($usersFirstname as $id => $firstname) {
			$users[$id] = $firstname.' '.$usersLastname[$id]; 
		}		
		return $users;						
	}

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

	/**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException('You can only login
							by username/password pair for now.');
    }

 
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
         return $this->getAuthKey() === $authKey;
    }	

	public function validatePassword($password)
	{
		return $this->isCorrectHash($password, $this->password); 
	}

	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}

    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
}
